### variables
variable "access_key" {}
variable "secret_key" {}
variable "region" {}
variable "token" {}

# provider AWS
provider "aws" {
  region     = var.region
  access_key = var.access_key
  secret_key = var.secret_key
  token      = var.token
}

### private VPC
resource "aws_vpc" "nginx_exercise_vpc" {
  cidr_block = "10.0.0.0/16"
}

### public subnet VPC - zone A
resource "aws_subnet" "nginx_exercise_subnet_public_az_a" {
  vpc_id            = aws_vpc.nginx_exercise_vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "eu-west-1a"
}

### public subnet VPC - zone B
resource "aws_subnet" "nginx_exercise_subnet_public_az_b" {
  vpc_id            = aws_vpc.nginx_exercise_vpc.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "eu-west-1b"
}

### internet GW
resource "aws_internet_gateway" "nginx_exercise_gateway" {
  vpc_id = aws_vpc.nginx_exercise_vpc.id
}

### VPC to GW
resource "aws_route_table" "nginx_exercise_route_table" {
  vpc_id = aws_vpc.nginx_exercise_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.nginx_exercise_gateway.id
  }
}

### public subnet to RT - zone A
resource "aws_route_table_association" "nginx_exercise_route_table_association_public_az_a" {
  subnet_id      = aws_subnet.nginx_exercise_subnet_public_az_a.id
  route_table_id = aws_route_table.nginx_exercise_route_table.id
}

### public subnet to RT - zone B
resource "aws_route_table_association" "nginx_exercise_route_table_association_public_az_b" {
  subnet_id      = aws_subnet.nginx_exercise_subnet_public_az_b.id
  route_table_id = aws_route_table.nginx_exercise_route_table.id
}

### SG Load Balancer
resource "aws_security_group" "nginx_exercise_lb_security_group" {
  name        = "nginx-exercise-lb-security-group"
  description = "Allow traffic from nginx-exercise-security-group"
  vpc_id      = aws_vpc.nginx_exercise_vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [
      "52.64.200.255/32",
      "18.204.96.68/32",
      "3.72.159.139/32",
      "185.107.236.8/32",
      "149.106.185.179/32",
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

### SG EC2
resource "aws_security_group" "nginx_exercise_security_group" {
  name        = "nginx-exercise-security-group"
  description = "Allow SSH and HTTP traffic"
  vpc_id      = aws_vpc.nginx_exercise_vpc.id

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.nginx_exercise_lb_security_group.id]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

### EC2 Ubuntu 22.04 private subnet - zone A
resource "aws_instance" "nginx_exercise_instance_az_a" {
  ami           = "ami-01dd271720c1ba44f"
  instance_type = "t2.micro"
  vpc_security_group_ids = [
    aws_security_group.nginx_exercise_security_group.id,
    aws_security_group.nginx_exercise_lb_security_group.id,
  ]
  subnet_id     = aws_subnet.nginx_exercise_subnet_public_az_a.id
  key_name      = "jsklenar"

  user_data = <<-EOF
              #!/bin/bash
              sudo apt-get update
              sudo apt-get install -y docker.io
              sudo docker run -d -p 80:80 nginx
              EOF
}

### EC2 Ubuntu 22.04 private subnet - zone B
resource "aws_instance" "nginx_exercise_instance_az_b" {
  ami           = "ami-01dd271720c1ba44f"
  instance_type = "t2.micro"
  vpc_security_group_ids = [
    aws_security_group.nginx_exercise_security_group.id,
    aws_security_group.nginx_exercise_lb_security_group.id,
  ]
  subnet_id     = aws_subnet.nginx_exercise_subnet_public_az_b.id
  key_name      = "jsklenar"

  user_data = <<-EOF
              #!/bin/bash
              sudo apt-get update
              sudo apt-get install -y docker.io
              sudo docker run -d -p 80:80 nginx
              EOF
}

### AWS Elastic IP - zone A
resource "aws_eip" "nginx_exercise_eip_az_a" {
  instance = aws_instance.nginx_exercise_instance_az_a.id
}

### AWS Elastic IP - zone B
resource "aws_eip" "nginx_exercise_eip_az_b" {
  instance = aws_instance.nginx_exercise_instance_az_b.id
}

### Target Group
resource "aws_lb_target_group" "nginx_exercise_target_group" {
  name     = "nginx-exercise-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.nginx_exercise_vpc.id
}

### Instances To Target Group
resource "aws_lb_target_group_attachment" "nginx_exercise_target_group_attachment_az_a" {
  target_group_arn = aws_lb_target_group.nginx_exercise_target_group.arn
  target_id        = aws_instance.nginx_exercise_instance_az_a.id
  port             = 80
}

resource "aws_lb_target_group_attachment" "nginx_exercise_target_group_attachment_az_b" {
  target_group_arn = aws_lb_target_group.nginx_exercise_target_group.arn
  target_id        = aws_instance.nginx_exercise_instance_az_b.id
  port             = 80
}

### Load Balancer Listener
resource "aws_lb_listener" "front_end_az_a" {
  load_balancer_arn = aws_lb.nginx_exercise_load_balancer.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.nginx_exercise_target_group.arn
    type             = "forward"
  }
}

### Load Balancer
resource "aws_lb" "nginx_exercise_load_balancer" {
  name               = "nginx-exercise-load-balancer"
  security_groups    = [aws_security_group.nginx_exercise_lb_security_group.id]
  subnets            = [aws_subnet.nginx_exercise_subnet_public_az_a.id, aws_subnet.nginx_exercise_subnet_public_az_b.id]
  internal           = false
  load_balancer_type = "application"
}

### Output For IP
output "instance_ip_az_a" {
  value = aws_instance.nginx_exercise_instance_az_a.public_ip
}

output "instance_ip_az_b" {
  value = aws_instance.nginx_exercise_instance_az_b.public_ip
}

output "load_balancer_dns" {
  value = aws_lb.nginx_exercise_load_balancer.dns_name
}
